// imports
// ******************************************
import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';
import VueMoment from 'vue-moment';

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(VueMoment);

import schedulingPage from "pages/scheduling.vue";

import appointment from "../components/appointment/appointment.vue";
import journeyMap from "../components/journey-map/journey-map.vue";

const router = new VueRouter({
  routes: [
    // { path: '/', redirect: '/scheduling' },
    // {
    //   path: '/scheduling',
    //   name: 'scheduling',
    //   component: schedulingPage,
    // },
    // {
    //   path: '/case-managment',
    //   name: 'case-managment',
    //   component: caseManagment,
    // },
    // dynamic
    // { path: '/video/:id', component: videoView },
    // { path: '/favourite', component: favouritePage },
  ],
});

// import css style to app
import '../scss/main.scss';

import '../svg-sprite/_svg-strite';


import sidebarPacient from "../components/sidebar/sidebar.vue";
import patientInfo from "../components/patient-info/patient-info.vue";
import pharmacy from "../components/pharmacy/pharmacy.vue";
import insuranceVerification from "../components/insurance-verification/insurance-verification.vue";
import makeAppointment from "../components/make-appointment/make-appointment.vue";
import cManagment from "../components/case-managment/case-managment.vue";
import cManagmentIncidents from "../components/case-managment/case-managment-incidents.vue";
import cManagmentIncident from "../components/case-managment/case-managment-incident.vue";



let appData = {
  activePacient: null,

  currentShowBox: null,
  currentShowSubBox: null,
};

let App = new Vue({
  data: appData,
  router,
  created(){
    let vm = this;
    vm.activePacient = 0;
  },
  mounted() {
    let vm = this;
    vm.currentShowBox = 'scheduling';
    vm.spaceWidget =  window.ciscosparkClient();
    Vue.http.get('demo-credentials.json').then((response) => 
    {
      console.log(vm.spaceWidget.init);
      vm.spaceWidget.init(response.data);
    });
  },
  methods: {
    showSpaceWidget: function () {
      var vm = this;
      var $el = document.getElementById("huddle-room-mount");
      if($el){
        console.log(vm.spaceWidget);
        vm.spaceWidget.render($el);
      }
    }
  },
  components: {
    sidebarPacient,
    appointment,
    journeyMap,
    patientInfo,
    pharmacy,
    insuranceVerification,
    makeAppointment,
    cManagment,
    cManagmentIncidents,
    cManagmentIncident
  },
  watch: {
    currentShowBox: function () {
      this.currentShowSubBox = null;
    }
  }
});


// ajax send data form to url - get list ID's
Vue.http.get('demo-mockup.json')
// get access
  .then(
    (response) => {
      console.log(response);
      appData = Object.assign(appData, response.body);
      App.$mount('#app');
    });

// ajax send data form to url - get list ID's
